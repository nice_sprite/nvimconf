local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup({
    'nvim-tree/nvim-tree.lua',
    'nvim-lua/plenary.nvim',
    'nvim-telescope/telescope.nvim',
    'nvim-telescope/telescope-fzf-native.nvim',
    'hrsh7th/nvim-cmp',
    'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-path',
    'tpope/vim-surround',
    {
        'nvim-lualine/lualine.nvim',
        dependencies = {
            'nvim-tree/nvim-web-devicons'
        },
        config = function()
            require('lualine').setup {
                options = {
                    icons_enabled = true,
                    theme = 'ayu_dark',
                    component_separators = { left = '', right = ''},
                    section_separators = { left = '', right = ''},
                    disabled_filetypes = {
                        statusline = {},
                        winbar = {},
                    },
                    ignore_focus = {},
                    always_divide_middle = true,
                    globalstatus = false,
                    refresh = {
                        statusline = 1000,
                        tabline = 1000,
                        winbar = 1000,
                    }
                },
                sections = {
                    lualine_a = {'mode'},
                    lualine_b = {'branch', 'diff', 'diagnostics'},
                    lualine_c = {'filename'},
                    lualine_x = {'fileformat', 'filetype'},
                    lualine_y = {'progress'},
                    lualine_z = {'location'}
                },
                inactive_sections = {
                    lualine_a = {},
                    lualine_b = {},
                    lualine_c = {'filename'},
                    lualine_x = {'location'},
                    lualine_y = {},
                    lualine_z = {}
                },
                tabline = {},
                winbar = {},
                inactive_winbar = {},
                extensions = {}
            }        
        end
    },
    'neovim/nvim-lspconfig',
    'hrsh7th/cmp-nvim-lsp',
    {
        'glepnir/lspsaga.nvim',
        config = function()
            require('lspsaga').setup{
            }
        end,
        dependencies =  {
            'nvim-treesitter/nvim-treesitter', -- optional
            'nvim-tree/nvim-web-devicons',     -- optional
        }
    },
    'hrsh7th/cmp-nvim-lsp',
    'windwp/nvim-autopairs',
    'bluz71/vim-moonfly-colors',
    'srcery-colors/srcery-vim',
    'L3MON4D3/LuaSnip',
    'rust-lang/rust.vim',
    'EdenEast/nightfox.nvim',
    'wuelnerdotexe/vim-enfocado',
    'rmagatti/auto-session',
    {
        'Saecki/crates.nvim',
        tag = 'stable',
        config = function()
            require'crates'.setup()
        end
    },
    {
        "~p00f/clangd_extensions.nvim",
        url = "https://git.sr.ht/~p00f/clangd_extensions.nvim",
    },
    {
        'ThePrimeagen/harpoon',
        config = function()
            require'harpoon'.setup{}
        end
    },
    {
        'nvim-treesitter/nvim-treesitter',
        config = function()
            require'nvim-treesitter.configs'.setup{
                ensure_installed = {'lua', 'odin', 'cpp'},
                auto_install = false,
                highlight = { 
                    enable = true,
                    additional_vim_regex_highlighting = false,
                }
            }
        end

    },
    {
        'folke/tokyonight.nvim'
    },
    {
        'navarasu/onedark.nvim'
    },
    {
        "christoomey/vim-tmux-navigator"
    }
}, opts)

function Transparency(transparent)
    if transparent ~= nil then
        vim.cmd('highlight Normal guibg=#000000') 
        vim.cmd('highlight NormalNC guibg=#000000') 
        vim.cmd [[highlight VertSplit guifg=#00beff guibg=NONE]]
        vim.cmd [[highlight VertSplit guifg=#00beff guibg=NONE]]
        -- Set the background to transparent
        vim.cmd [[highlight Normal guibg=NONE ctermbg=NONE]]
        vim.cmd [[highlight NormalNC guibg=NONE ctermbg=NONE]]
        vim.cmd [[highlight NonText guibg=NONE ctermbg=NONE]]
        vim.cmd [[highlight LineNr guibg=NONE ctermbg=NONE]]
        vim.cmd [[highlight Folded guibg=NONE ctermbg=NONE]]
        vim.cmd [[highlight EndOfBuffer guibg=NONE ctermbg=NONE]]
    end
end

function BackgroundOverride(override)
    if override ~= nil then
        vim.cmd [[highlight Normal guibg=#0a0a0a ]]
        vim.cmd [[highlight NormalNC guibg=#0a0a0a ]]
        vim.cmd [[highlight NonText guibg=#0a0a0a ]]
        vim.cmd [[highlight LineNr guibg=#0a0a0a ]]
        vim.cmd [[highlight Folded guibg=#0a0a0a ]]
        vim.cmd [[highlight EndOfBuffer guibg=NONE ctermbg=NONE]]
    end
end

Transparency(nil)
vim.api.nvim_create_autocmd("ColorScheme", {
    pattern = '*',
    callback = function()
        BackgroundOverride(true)
        Transparency(nil)
    end
})

--[[
--build system stuff
--]]
vim.opt.makeprg = './build.zsh build'

vim.g.mapleader = ' '
-- Allow moving between windows with just ctrl + hjkl
vim.cmd('nmap <silent> <C-h> :wincmd h<CR>');
vim.cmd('nmap <silent> <C-k> :wincmd k<CR>');
vim.cmd('nmap <silent> <C-j> :wincmd j<CR>');
vim.cmd('nmap <silent> <C-l> :wincmd l<CR>');

vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.smartindent = true
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.smartcase = true
vim.opt['guicursor'] = ''
vim.opt.wrap = false
vim.opt.cursorline = true
vim.opt.cursorlineopt = 'number'
vim.g.t_Co=256
vim.opt.linespace = 4
vim.opt.termguicolors = true
local colorscheme = 'onedark'
vim.cmd('colorscheme ' .. colorscheme)
vim.cmd[[set relativenumber]]

-- vim.cmd[[highlight Normal guibg=none]]
-- vim.cmd[[highlight NonText guibg=none]]
-- vim.cmd[[highlight Normal ctermbg=none]]
-- vim.cmd[highlight NonText ctermbg=none]]
vim.g.rustfmt_autosave = 1
vim.g.loaded_matchparen = 1 -- disable really slow and shitty paren matching

-- Customize the color of the vertical line separator
vim.cmd [[highlight VertSplit guifg=#00beff guibg=NONE]]

-- You might also want to disable the vertical splits for a cleaner look
vim.opt.laststatus = 2
-- vim.opt.showtabline = 2
vim.api.nvim_create_autocmd({'TextYankPost'}, {
    pattern = '*',
    callback= function()
        print('hi')
        vim.highlight.on_yank{higroup='Visual', timeout=300}
    end
})

-- vim.api.nvim_create_autocmd("BufWritePost", {
--   pattern = "*.odin",
--   callback = function()
--     if vim.bo.filetype == "odin" then
--         vim.lsp.buf.format()
--     end
--   end,
--   group = vim.api.nvim_create_augroup("OdinSave", { clear = true }),
-- 
-- })



if vim.g.neovide then
    vim.o.guifont = "iosevka nf:h12"
    vim.g.neovide_refresh_rate = 240
end

require("auto-session").setup {
    log_level = "error",
    auto_session_suppress_dirs = { "~/", "~/Projects", "~/Downloads", "/"},
}



require('nvim-autopairs').setup()
require('cmp').event:on('confirm_done', require('nvim-autopairs.completion.cmp').on_confirm_done()) 


telescope_keybinds = {
    -- normal mode
    n = {
        ["<leader>ff"] = { "<cmd> Telescope find_files <CR>", "Find files" },
        ["<leader>fa"] = { "<cmd> Telescope find_files follow=true no_ignore=true hidden=true <CR>", "Find all" },
        ["<leader>fw"] = { "<cmd> Telescope live_grep <CR>", "Live grep" },
        ["<leader>fb"] = { "<cmd> Telescope buffers <CR>", "Find buffers" },
        ["<leader>fh"] = { "<cmd> Telescope help_tags <CR>", "Help page" },
        ["<leader>fo"] = { "<cmd> Telescope oldfiles <CR>", "Find oldfiles" },
        ["<leader>fz"] = { "<cmd> Telescope current_buffer_fuzzy_find <CR>", "Find in current buffer" },
        ["<leader>fs"] = { "<cmd> Telescope lsp_document_symbols<CR>"},
        -- git
        ["<leader>cm"] = { "<cmd> Telescope git_commits <CR>", "Git commits" },
        ["<leader>gt"] = { "<cmd> Telescope git_status <CR>", "Git status" },

        -- pick a hidden term
        ["<leader>pt"] = { "<cmd> Telescope terms <CR>", "Pick hidden term" },


        ["<leader>ma"] = { "<cmd> Telescope marks <CR>", "telescope bookmarks" },
    }

}

nvimtree_keybinds = {
  n = {
    -- toggle
    ["<C-n>"] = { "<cmd> NvimTreeToggle <CR>", "Toggle nvimtree" },

    -- focus
    ["<leader>e"] = { "<cmd> NvimTreeFocus <CR>", "Focus nvimtree" },
  },

}

lspconfig_keybinds = {
  -- See `<cmd> :help vim.lsp.*` for documentation on any of the below functions

  n = {
    ["gD"] = {
      function()
        vim.lsp.buf.declaration()
      end,
      "LSP declaration",
    },

    ["gd"] = {
      function()
        vim.lsp.buf.definition()
      end,
      "LSP definition",
    },

    ["K"] = {
      function()
        -- vim.lsp.buf.hover()
        vim.cmd[[Lspsaga hover_doc]]
      end,
      "LSP hover",
    },

    ["gi"] = {
      function()
        vim.lsp.buf.implementation()
      end,
      "LSP implementation",
    },

    ["<leader>ls"] = {
      function()
        vim.lsp.buf.signature_help()
      end,
      "LSP signature help",
    },

    ["<leader>D"] = {
      function()
        vim.lsp.buf.type_definition()
      end,
      "LSP definition type",
    },

    ["<leader>ra"] = {
      function()
        -- require("nvchad.renamer").open()
        vim.lsp.buf.rename()
      end,
      "LSP rename",
    },

    ["<leader>ca"] = {
      function()
        vim.lsp.buf.code_action()
      end,
      "LSP code action",
    },

    ["gr"] = {
      function()
        vim.lsp.buf.references()
      end,
      "LSP references",
    },

    ["<leader>lf"] = {
      function()
        vim.diagnostic.open_float { border = "rounded" }
      end,
      "Floating diagnostic",
    },

    ["[d"] = {
      function()
        vim.diagnostic.goto_prev { float = { border = "rounded" } }
      end,
      "Goto prev",
    },

    ["]d"] = {
      function()
        vim.diagnostic.goto_next { float = { border = "rounded" } }
      end,
      "Goto next",
    },

    ["<leader>q"] = {
      function()
        vim.diagnostic.setloclist()
      end,
      "Diagnostic setloclist",
    },

    ["<leader>wa"] = {
      function()
        vim.lsp.buf.add_workspace_folder()
      end,
      "Add workspace folder",
    },

    ["<leader>wr"] = {
      function()
        vim.lsp.buf.remove_workspace_folder()
      end,
      "Remove workspace folder",
    },

    ["<leader>wl"] = {
      function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
      end,
      "List workspace folders",
    },
  },

  v = {
    ["<leader>ca"] = {
      function()
        vim.lsp.buf.code_action()
      end,
      "LSP code action",
    },
  },
}


function load_keybinds(binds) 
    for mode,bind_table in pairs(binds) do
        for key, action in pairs(bind_table) do
            vim.keymap.set(mode, key, action[1])
        end
    end
end

-- Example keybindings for harpoon
vim.api.nvim_set_keymap('n', '<leader>a', ":lua require('harpoon.mark').add_file()<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<leader>h', ":lua require('harpoon.ui').toggle_quick_menu()<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<leader>1', ":lua require('harpoon.ui').nav_file(1)<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<leader>2', ":lua require('harpoon.ui').nav_file(2)<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<leader>3', ":lua require('harpoon.ui').nav_file(3)<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<leader>4', ":lua require('harpoon.ui').nav_file(4)<CR>", { noremap = true, silent = true })

local nvimtree_options = {
  filters = {
    dotfiles = false,
    exclude = { vim.fn.stdpath "config" .. "/lua/custom" },
  },
  disable_netrw = true,
  hijack_netrw = true,
  hijack_cursor = true,
  hijack_unnamed_buffer_when_opening = false,
  sync_root_with_cwd = true,
  update_focused_file = {
    enable = true,
    update_root = false,
  },
  view = {
    adaptive_size = false,
    side = "left",
    width = 30,
    preserve_window_proportions = true,
  },
  git = {
    enable = false,
    ignore = true,
  },
  filesystem_watchers = {
    enable = true,
  },
  actions = {
    open_file = {
      resize_window = true,
    },
  },
  renderer = {
    root_folder_label = false,
    highlight_git = false,
    highlight_opened_files = "none",

    indent_markers = {
      enable = false,
    },

    icons = {
      show = {
        file = true,
        folder = true,
        folder_arrow = true,
        git = false,
      },

      glyphs = {
        default = "󰈚",
        symlink = "",
        folder = {
          default = "",
          empty = "",
          empty_open = "",
          open = "",
          symlink = "",
          symlink_open = "",
          arrow_open = "",
          arrow_closed = "",
        },
        git = {
          unstaged = "✗",
          staged = "✓",
          unmerged = "",
          renamed = "➜",
          untracked = "★",
          deleted = "",
          ignored = "◌",
        },
      },
    },
  },
}

require('nvim-tree').setup(nvimtree_options)

local telescope_options = {
  defaults = {
    vimgrep_arguments = {
      "rg",
      "-L",
      "--color=never",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
      "--smart-case",
    },
    prompt_prefix = "   ",
    selection_caret = "  ",
    entry_prefix = "  ",
    initial_mode = "insert",
    selection_strategy = "reset",
    sorting_strategy = "ascending",
    layout_strategy = "horizontal",
    layout_config = {
      horizontal = {
        prompt_position = "top",
        preview_width = 0.55,
        results_width = 0.8,
      },
      vertical = {
        mirror = false,
      },
      width = 0.87,
      height = 0.80,
      preview_cutoff = 120,
    },
    file_sorter = require("telescope.sorters").get_fuzzy_file,
    file_ignore_patterns = { "node_modules" },
    generic_sorter = require("telescope.sorters").get_generic_fuzzy_sorter,
    path_display = { "truncate" },
    winblend = 0,
    border = {},
    borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
    color_devicons = true,
    set_env = { ["COLORTERM"] = "truecolor" }, -- default = nil,
    file_previewer = require("telescope.previewers").vim_buffer_cat.new,
    grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
    qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new,
    -- Developer configurations: Not meant for general override
    buffer_previewer_maker = require("telescope.previewers").buffer_previewer_maker,
    mappings = {
      n = { ["q"] = require("telescope.actions").close },
    },
  },

  extensions_list = { "themes", "terms", "fzf" },
  extensions = {
    fzf = {
      fuzzy = true,
      override_generic_sorter = true,
      override_file_sorter = true,
      case_mode = "smart_case",
    },
  },
}

require('telescope').setup(telescope_options)


load_keybinds(telescope_keybinds)
load_keybinds(nvimtree_keybinds)
load_keybinds(lspconfig_keybinds)

-- LSP stuff
local zls_caps = require('cmp_nvim_lsp').default_capabilities()
require('lspconfig').zls.setup{
    cmd = {
        "/home/nice_sprite/zls/zig-out/bin/zls"
    },
    -- on_attach = function(client, bufnr)
    --     on_attach(client, bufnr)
    -- end,
    capabilities = zls_caps,
    
}

local util = require('lspconfig/util')
local ra_caps = require('cmp_nvim_lsp').default_capabilities()
require('lspconfig').rust_analyzer.setup {
    capabilities = ra_caps,
    filetypes = {"rust"},
    root_dir = util.root_pattern("Cargo.toml"),
    settings = {
        ['rust-analyzer'] = {
            cargo = {
                allFeatures = true,
            }
        }
    }
}

local clang_caps = require('cmp_nvim_lsp').default_capabilities()
require'lspconfig'.clangd.setup{
    capabilities = clang_caps, 
    cmd = {"clangd", "--completion-style=detailed", "--function-arg-placeholders=1"}
}


local odin_caps = require('cmp_nvim_lsp').default_capabilities()
require'lspconfig'.ols.setup{
    capabilities = odin_caps, 
}

require'lspconfig'.terraformls.setup{}
vim.api.nvim_create_autocmd({"BufWritePre"}, {
  pattern = {"*.tf", "*.tfvars"},
  callback = function()
    vim.lsp.buf.format()
  end,
})

local glsl_caps = require('cmp_nvim_lsp').default_capabilities()
require('lspconfig').glsl_analyzer.setup { 
    capabilities = glsl_caps,
    filetypes = {"glsl", "vert", "tesc", "tese", "frag", "geom", "comp"},
    single_file_support = true,
}


local cmp = require('cmp')
local field_arrangement = {
  atom = { "kind", "abbr", "menu" },
  atom_colored = { "kind", "abbr", "menu" },
}

local formatting_style = {
  -- default fields order i.e completion word + item.kind + item.kind icons
  fields = field_arrangement[cmp_style] or { "abbr", "kind", "menu" },

  -- format = function(_, item)
  --   local icons = require "nvchad.icons.lspkind"
  --   local icon = (cmp_ui.icons and icons[item.kind]) or ""

  --   if cmp_style == "atom" or cmp_style == "atom_colored" then
  --     icon = " " .. icon .. " "
  --     item.menu = cmp_ui.lspkind_text and "   (" .. item.kind .. ")" or ""
  --     item.kind = icon
  --   else
  --     icon = cmp_ui.lspkind_text and (" " .. icon .. " ") or icon
  --     item.kind = string.format("%s %s", icon, cmp_ui.lspkind_text and item.kind or "")
  --   end

  --   return item
  -- end,
}

local function border(hl_name)
  return {
    { "╭", hl_name },
    { "─", hl_name },
    { "╮", hl_name },
    { "│", hl_name },
    { "╯", hl_name },
    { "─", hl_name },
    { "╰", hl_name },
    { "│", hl_name },
  }
end

local cmp_options = {
  completion = {
    completeopt = "menu,menuone",
  },

  -- window = {
  --   completion = {
  --     side_padding = (cmp_style ~= "atom" and cmp_style ~= "atom_colored") and 1 or 0,
  --     winhighlight = "Normal:CmpPmenu,CursorLine:CmpSel,Search:None",
  --     scrollbar = false,
  --   },
  --   documentation = {
  --     border = border "CmpDocBorder",
  --     winhighlight = "Normal:CmpDoc",
  --   },
  -- },
  snippet = {
    expand = function(args)
      require("luasnip").lsp_expand(args.body)
    end,
  },


  mapping = cmp.mapping.preset.insert{
    ["<C-p>"] = cmp.mapping.select_prev_item(),
    ["<C-n>"] = cmp.mapping.select_next_item(),
    ["<C-d>"] = cmp.mapping.scroll_docs(-4),
    ["<C-f>"] = cmp.mapping.scroll_docs(4),
    ["<C-Space>"] = cmp.mapping.complete(),
    ["<C-e>"] = cmp.mapping.close(),
    ["<CR>"] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Insert,
      select = true,
    },
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif require("luasnip").expand_or_jumpable() then
        vim.fn.feedkeys(vim.api.nvim_replace_termcodes("<Plug>luasnip-expand-or-jump", true, true, true), "")
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif require("luasnip").jumpable(-1) then
        vim.fn.feedkeys(vim.api.nvim_replace_termcodes("<Plug>luasnip-jump-prev", true, true, true), "")
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
  },
  sources = {
    { name = "nvim_lsp" },
    { name = "luasnip" },
    { name = "buffer" },
    { name = "nvim_lua" },
    { name = "path" },
  },
}


cmp.setup(cmp_options)
-- vim.opt.fillchars = {
--     vert = "▕",
--     eob = " ",
--     foldsep = "|",
-- }
-- 

